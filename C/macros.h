#include <stdio.h>


// Output macros
#define PI(X) printf("%d\t", X);
#define PIp(X) printf("%d\t", *X);
#define PF(X) printf("%f\t", X);
#define PFp(X) printf("%f\t", *X);
#define PC(X) printf("%s\t", X);
#define PV(X) printf(#X);
#define PS(X) printf(X);
#define NL PS("\n");


// Input macros
#define s(n)                        scanf("%d",&n)
#define sc(n)                       scanf("%c",&n)
#define sl(n)                       scanf("%lld",&n)
#define sf(n)                       scanf("%lf",&n)
#define ss(n)                       scanf("%s",n)


// Useful constants
#define INF                         (int)1e9
#define EPS                         1e-9


// Useful hardware instructions
#define bitcount                    __builtin_popcount
#define gcd                         __gcd


// Useful container manipulation / traversal macros
#define forall(i,a,b)               for(int i=a;i<b;i++)
#define ForEach(v, c)               for( typeof( (c).begin()) v = (c).begin();  v != (c).end(); ++v)
#define all(a)                      a.begin(), a.end()
#define in(a,b)                     ( (b).find(a) != (b).end())
#define pb                          push_back
#define fill(a,v)                    memset(a, v, sizeof a)
#define sz(a)                       ((int)(a.size()))
#define mp                          make_pair
#define fi first
#define se second
#define X real() // useful for working with #include <complex> for computational geometry
#define Y imag()


// iteration macros: have advantage of auto-casting and not recomputing arguments
#define rep(i,n) for(int i=0, _##i=(n); i<_##i; ++i)
#define dwn(i,n) for(int i=(n); --i>=0; )
#define repr(i,l,r) for(int i=(l), _##i=(r); i<_##i; ++i)
#define dwnr(i,l,r) for(int i=(r), _##i=(l); --i>=_##i; )
#define repi(i,a) for(__typeof((a).begin()) i=(a).begin(), _##i=(a).end(); i!=_##i; ++i)
#define dwni(i,a) for(__typeof((a).rbegin()) i=(a).rbegin(), _##i=(a).rend(); i!=_##i; ++i)


// Some common useful functions
#define maX(a,b)                     ( (a) > (b) ? (a) : (b))
#define miN(a,b)                     ( (a) < (b) ? (a) : (b))
#define checkbit(n,b)                ( (n >> b) & 1)
#define DREP(a)                      sort(all(a)); a.erase(unique(all(a)),a.end())
#define INDEX(arr,ind)               (lower_bound(all(arr),ind)-arr.begin())
#define CONCATENATE(arg1, arg2) arg1##arg2
#define STR(x)   #x

#define SHOW_DEFINE(x) printf("%s \t= \t%s\n", #x, STR(x))

#define CRIS -6
#define FRED 1
#define HARRY FRED
#define BOB ON_HOLIDAY
#define WALLY

#define IMPLIES(x, y) (!(x) || (y))

#define COMPARE(x, y) (((x) > (y)) - ((x) < (y)))
#define SIGN(x) COMPARE(x, 0)

#define SWAP(x, y, T) do \
				{ \
					T tmp = (x); \
					(x) = (y); \
					(y) = tmp; \
				} while(0)
#define SORT2(a, b, T) do \
				{ \
					if ((a) > (b)) \
						SWAP((a), (b), T); \
				} while (0)

#define SET(d, n, v) do \
				{ \
					size_t i_, n_; \
					for (n_ = (n), i_ = 0; n_ > 0; --n_, ++i_) \
						(d)[i_] = (v); \
				} while(0)
#define ZERO(d, n) SET(d, n, 0)

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*a))
#define foreach(item, array) \
		for(int keep=1, count=0, size=ARRAY_SIZE(array); \
        		keep && count != size; \
        		keep = !keep, count++) \
    	for(item = (array)+count; keep; keep = !keep)

#define FOR(I, LEN) \
      	for (int I = 0; I < LEN; ++I)

// Finite State Machine implementaion is a breeze with the following 3 macros
      		//see the example in the main function
#define FSM            for(;;)
#define STATE(x)       x##_s 
#define NEXTSTATE(x)   goto x##_s

/// Return min of two numbers. Commonly used but never defined as part of standard headers
#ifndef MIN
#define MIN( n1, n2 )   ((n1) > (n2) ? (n2) : (n1))
#endif
/// Return max of two numbers. Commonly used but never defined as part of standard headers
#ifndef MAX
#define MAX( n1, n2 )   ((n1) > (n2) ? (n1) : (n2))
#endif

//Sometimes when we allocate a memory pool, we might want the size to be a perfect power of two 
      		//and following two macros could be useful for such cases.
// Aligns the supplied size to the specified PowerOfTwo
#define ALIGN_SIZE( sizeToAlign, PowerOfTwo ) \
        (((sizeToAlign) + (PowerOfTwo) - 1) & ~((PowerOfTwo) - 1))
 
// Checks whether the supplied size is aligned to the specified PowerOfTwo
#define IS_SIZE_ALIGNED( sizeToTest, PowerOfTwo ) \
        (((sizeToTest) & ((PowerOfTwo) - 1)) == 0)

/**
To open a "C/C++" block without using any construct such as "if", "for",
"while", etc.
 
The main purpose of this macro is to improve readability and to make the
intentions clear in the code.
 
This is useful if some local variables are required only for few lines.
In such cases putting such local variables in a block causes the local
variables to go out of scope and hence reclaim their memory once the end
of block is reached.
*/
#define BEGIN_BLOCK {
 
/**
Closes a "C/C++" block opened using BEGIN_BLOCK.
*/
#define END_BLOCK   }

/**
Use this macro for unused parameters right in the beginning of a function body
to suppress compiler warnings about unused parameters.
 
This is mainly meant for function parameters and not for unused local variables.
*/
#define UNUSED( ParamName ) \
        ((void)(0 ? ((ParamName) = (ParamName)) : (ParamName)))

/// Determine whether the given signed or unsigned integer is odd.
#define IS_ODD( num )   ((num) & 1)
 
/// Determine whether the given signed or unsigned integer is even.
#define IS_EVEN( num )  (!IS_ODD( (num) ))
 
/**
Determine whether the given number is between the other two numbers
(both inclusive).
*/
#define IS_BETWEEN( numToTest, numLow, numHigh ) \
        ((unsigned char)((numToTest) >= (numLow) && (numToTest) <= (numHigh)))


