#include "macros.h"
#include "matrix.h"

void 
/*
Input in M 		Transform in S 		Output
0 1 1 0 1 		0 1 1 0 1 			1 1 1 
1 1 0 1 0		1 1 0 1 0			1 1 1
0 1 1 1 0		0 1 1 1 0			1 1 1
1 1 1 1 0		1 1 2 2 0
1 1 1 1 1 		1 2 2 3 1
0 0 0 0 0		0 0 0 0 0
*/
printMaxSubSquare(int** M, int R, int C)
{
  int i,j;
  int S[R][C];
  int maximum, i_index_max, j_index_max;
  maximum = S[0][0]; i_index_max = 0; j_index_max = 0;
   
  /* Set first column of S[][]*/
  for(i = 0; i < R; i++)
     S[i][0] = M[i][0];
   
  /* Set first row of S[][]*/   
  for(j = 0; j < C; j++)
     S[0][j] = M[0][j];
       
    for(i = 1; i < R; i++)
    {
        for(j = 1; j < C; j++)
        {
            if(M[i][j] == 1)
                S[i][j] = MIN(S[i][j-1], MIN(S[i-1][j], S[i-1][j-1])) + 1;
            else
                S[i][j] = 0;
        }   
    }
   
    for(i = 0; i < R; i++)
    {
        for(j = 0; j < C; j++)
        {
            if(maximum < S[i][j])
            {
                maximum = S[i][j];
                i_index_max = i;
                j_index_max = j;
            }       
        }                
    }    
    
    PV(Maximum size sub-matrix is:)	NL
    for(i = i_index_max; i > i_index_max - maximum; i--)
    {
        for(j = j_index_max; j > j_index_max - maximum; j--)
        {
            PI(M[i][j])
        } 
        NL
    } 
}     

int 
main()
{
	int R =8, C=6;
	int **M; 
	int base = 2;

	M = MakeMatrix(R,C);
	RandomMatrix(M, base, R, C);
	PrintMatrix(M, R, C);
	printMaxSubSquare(M, R, C);
}