#include<stdio.h>
#include<stdlib.h>

#include "macros.h"
#define SZ 6

void
f (int* arr, int st, int sz, int pos = -1)
{
	if (sz == 0)	//well, this is the point of return
		return;

	PI(pos)
	pos = (pos + ((st + 1) % sz)) % SZ; 
	PI(pos)
	while (arr[pos] != -1)
		pos = (pos + 1) % SZ;
	PI(pos) PV (-->)
	PI(st) NL
	arr[pos] = st;
	f (arr, st + 1, sz - 1, pos);
}

void
//prints the array of size
PrintArray(int* arr, int size)
{
	for (int i = 0; i < size; i++)	//we will print the final array now
		PI (arr [i]);
	NL
}

int
main()
{
	int st = 1;

	int* arr = (int*) calloc (SZ, sizeof(int));

	for (int i = 0; i < SZ; i++)
		arr[i] = -1;

	//f (arr, st, SZ);	//put things into perspective

	int rounds = 0; // move / left
	int offset;		// move % left
	int filled = 0;	// i
	int left = SZ;	// SZ - filled
	int move;		// i + 2
	int pos;		// to decide

	FOR(i, SZ)
	{
		move = i + 2; PI(move)
		rounds = move / left; PI(rounds)
		offset = move % left; PI(offset)
		left --; PI(left)
		filled ++; PI(filled)
		PV(-->)PI(i) NL
		
	}

	PrintArray(arr, SZ);

	//lets do the cleaning, shall we?
	free(arr);
	return 0;
}