#include <stdio.h>

#include "macros.h"

int 
main()
{
    SHOW_DEFINE(BERT);
    SHOW_DEFINE(CRIS);
    SHOW_DEFINE(FRED);
    SHOW_DEFINE(HARRY);
    SHOW_DEFINE(BOB);
    SHOW_DEFINE(WALLY);

    int arr[] = {1, 2, 3, 4, 5, 6};
    foreach(int const* c, arr)	PI(*c)	NL

    int mul[][2] = { { 1, 2 }, { 3, 4 } };
    foreach(int (*c1)[2], mul)	{foreach(int *c2, *c1)	PI(*c2)	NL} NL

    int n = 2;
    PV(Implies output:) PI(IMPLIES(n > 0, arr != NULL))	NL

    /*
    FSM {
    STATE(s1):
      ... do stuff ...
      NEXTSTATE(s2);

    STATE(s2):
      ... do stuff ...
      if (k<0) NEXTSTATE(s2); // fallthrough as the switch() cases

    STATE(s3):
      ... final stuff ...
      break;  // Exit from the FSM
    }
    */

return 0;
}