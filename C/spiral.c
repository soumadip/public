#include "macros.h"
#include "matrix.h"

#define SZ 5

void
//take a square matrix as input and fill it up spirally
FillSpiral(int ** mat, int size)
{
	int i, j, k;
	int filler = 0;

    for(i = size - 1, j = 0; i > 0; --i, ++j)
    {
        //make top row
        for (k = j; k < i; ++k) 
        	mat[j][k] = ++filler;
        //make last column
        for (k = j; k < i; ++k) 
        	mat[k][i] = ++filler;
        //make last row
        for (k = i; k > j; --k) 
        	mat[i][k] = ++filler;
        //make first column
        for (k = i; k > j; --k) 
        	mat[k][j] = ++filler;
    }
    //if odd size matrix make the middle value
     int middle = (size - 1) / 2;
     if (size % 2 == 1) 
     	mat[middle][middle] = ++filler;
}


int 
main()
{
	int **matrix;

	matrix = MakeMatrix(SZ);
	FillSpiral(matrix, SZ);
   	PrintMatrix(matrix, SZ);
   	//FillMatrixWith(matrix, 0, SZ);
   	//PrintMatrix(matrix, SZ);
}