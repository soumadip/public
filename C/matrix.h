#include<stdio.h>
#include<stdlib.h>
#include<time.h>
void 
FillMatrixWith(int ** mat , int val, int r_size, int c_size = 0)
{
	if (!c_size)
		c_size = r_size;

	for (int i = 0; i < r_size; ++i)
		for (int j = 0; j < c_size; ++j)
			mat[i][j] = val;
}
void
RandomMatrix(int ** mat , int base, int r_size, int c_size = 0)
{
	if (!c_size)
		c_size = r_size;

	srand(time(NULL));
	for (int i = 0; i < r_size; ++i)
		for (int j = 0; j < c_size; ++j)
			mat[i][j] = rand() % base;
}

void 
PrintMatrix(int ** mat, int r_size, int c_size = 0)
{
	if (!c_size)
		c_size = r_size;
	for (int i = 0; i < r_size; ++i)
	{
		for (int j = 0; j < c_size; j++)
			PI(mat[i][j])
		NL
	}
}

int **
MakeMatrix(int r_size, int c_size = 0)
{
	int ** temp;

	if (!c_size)
		c_size = r_size;

	temp = (int **) malloc (sizeof(int *) * r_size);

	for (int i = 0; i < r_size; ++i)
		temp[i] = (int *) malloc (sizeof(int) * c_size);

	return temp;
}