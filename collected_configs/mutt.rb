#!/usr/bin/ruby 
# Copyright 2012, Geir Isene. Released under the GPL v. 3
# Version 0.2 (2012-04-24)


##################
#   Initialize   #
##################

require "ftools"
require 'net/imap'

# In .mail.conf, set the appropriate variables like this:
#
#  I_server1  = ""
#  I_user1    = ""
#  I_pass1    = ""
#
# Iserver0 would be "localhost" for mail delivery to local IMAP server
# Use as many servers you need with I_serverX, I_userX and I_passX
load    '~/.mail.conf'

$count = 0


###########################################
#   Define main Fetch & Filter function   #
###########################################

def matching (match, match_in, to_box, del)
    res = []
    message = ""
    to_box = "INBOX." + to_box
    if match_in =~ /s/ then res = res + $imap_from.search(["UNSEEN", "SUBJECT", match]) end
    if match_in =~ /b/ then res = res + $imap_from.search(["UNSEEN", "BODY", match]) end
    if match_in =~ /f/ then res = res + $imap_from.search(["UNSEEN", "FROM", match]) end
    if match_in =~ /t/ then res = res + $imap_from.search(["UNSEEN", "TO", match]) end
    if match_in =~ /c/ then res = res + $imap_from.search(["UNSEEN", "CC", match]) end
    if match_in =~ /h/ then res = res + $imap_from.search(["UNSEEN", "HEADER", match]) end
    if match_in =~ /a/ then res = res + $imap_from.search(["UNSEEN", "TEXT", match]) end
    res.uniq!
    res.each do |message_id|
      message = $imap_from.fetch(message_id,'RFC822')[0].attr['RFC822']
      $imap_to.append(to_box, message)
      if del == 1
	  $imap_from.store(message_id, "+FLAGS", [:Deleted])
      else 
	  $imap_from.store(message_id, "+FLAGS", [:Seen])
      end
      $count = $count + 1
    end
end


#######################################
#   Log into the target IMAP server   #
#######################################

$imap_to = Net::IMAP.new(I_server0, port="143")
$imap_to.login(I_user0, I_pass0)
$imap_to.select("INBOX")

###############################################################
#   Log into each "from"-server. Start Fetching & Filtering   #
###############################################################
#
# The syntax for matching is: 
# matching("string-to match", "match-against", "send-to-mailbox", "delete mail?)
# Set "delete mail?" to 1 if you want mail deleted from the source server.
#
# The following options are available to match against:
# "s" for SUBJECT, "b" for BODY, "t" for TO, "f" for FROM, "c" for CC
# to match any part of the mail (header or body): "a" for ALL

# From I_server1 (private GMail)
$imap_from = Net::IMAP.new(I_server1, port="993", usessl="true")
$imap_from.login(I_user1, I_pass1)
$imap_from.select("INBOX")

matching( "",				"s",	"Geir",		    0 )	# Catch rest

# Expunge mails that are set to be deleted and then disconnect
$imap_from.expunge
$imap_from.disconnect

# From I_server2 (FreeCode GMail)
$imap_from = Net::IMAP.new(I_server2, port="993", usessl="true")
$imap_from.login(I_user2, I_pass2)
$imap_from.select("INBOX")

matching( "efn-listen",			"tc",   "Lists.EFN",	    1 )
matching( "efn-agenda",			"tc",   "Lists.EFN-agenda", 1 )
matching( "styre@mailman.efn.no",	"tc",   "Lists.EFN-styret", 1 )

matching( "",				"s",    "FreeCode",	    0 )	# Catch rest

# Expunge mails that are set to be deleted and then disconnect
$imap_from.expunge
$imap_from.disconnect

# From I_server3 (FreeCode OLD)
$imap_from = Net::IMAP.new(I_server3, port="993", usessl="true")
$imap_from.login(I_user3, I_pass3)
$imap_from.select("INBOX")

matching( "ivy-subscribers",		"tc",	"Lists.IVY",	    1 )
matching( "FreezoneOrg@yahoogroups.com","tc",	"Lists.FZa",	    1 )
matching( "ifachat@yahoogroups.com",	"tc",	"Lists.FZa",	    1 )
matching( "koha",			"tc",   "Lists.Koha",	    1 )
matching( "nuug.no",			"tc",   "Lists.NUUG",	    1 )
matching( "linuxiskolen@skolelinux.no",	"tc",   "Lists.SLX",	    1 )
matching( "spirituellkultur",		"tc",	"Lists.AltMus",	    1 )
matching( "hhc@lists.brouhaha.com",	"tc",	"Lists.HHC",	    1 )

matching( "",				"s",	"Geir",		    0 )	# Catch rest

# Expunge mails that are set to be deleted and then disconnect
$imap_from.expunge
$imap_from.disconnect

# From I_server4 (FreeCode Int)
$imap_from = Net::IMAP.new(I_server4, port="993", usessl="true")
$imap_from.login(I_user4, I_pass4)
$imap_from.select("INBOX")

matching( "",				"s",	"FreeCode",	    0 )	# Catch rest

# Expunge mails that are set to be deleted and then disconnect
$imap_from.expunge
$imap_from.disconnect

##############################################################
#   Check for new mails in folders and write result to file  #
##############################################################

mailboxes = [
    [ "FC  : ",	"FreeCode"	    ],
    [ "Geir: ",	"Geir"		    ]
]

open('/home/noosisegei/.mail', 'w') do |f|
    mailboxes.each do |a|
	f.write( a[0] + $imap_to.status("INBOX." + a[1], "UNSEEN")["UNSEEN"].to_s + "\n" )
    end
end

# Copy file to another file to ensure no blinking in Conky
# Read the file from Conky to display new email in each folder
File.copy('/home/noosisegei/.mail','/home/noosisegei/.mail2')

#######################################################################
#   Disconnect from target server & Display number of mails filtered  #
#######################################################################

$imap_to.disconnect

puts "#{$count} mails filtered"

