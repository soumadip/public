# i3 config file (v4) (http://i3wm.org/docs/userguide.html)
#
# Geir Isene - g@isene.com - 2014-05-24

# basic settings
focus_follows_mouse yes
new_window 1pixel
new_float 1pixel
workspace_layout tabbed
default_orientation vertical
workspace_auto_back_and_forth yes

# variables
set $mod Mod4
set $alt Mod1
set $term urxvt +sb -fg white -bg black

# font for window titles. ISO 10646 = Unicode
font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+x exec $term
bindsym $mod+Return exec $term
bindsym $mod+Shift+Return exec $term -e sudo -s
#bindsym $alt+Return exec $term
#bindsym $alt+Shift+Return exec $term -e sudo -s

# kill focused window
bindsym $mod+q kill

# start dmenu (a program launcher)
bindsym F12 exec dmenu_run -nb "#fff" -nf "#000" -sb "#000" -sf "#fff"

# switch to next/prev workspace
bindsym $alt+Right workspace next
bindsym $alt+Left workspace prev
bind $alt+94 workspace prev
bind $alt+shift+94 workspace next

# change focus
bindsym $mod+Left focus left
bind $mod+94 focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right
bind $mod+shift+94 focus right

# move focused window
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h
bind $mod+167 split h

# split in vertical orientation
bindsym $mod+v split v
bind $mod+166 split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen

# change container layout (stacked, tabbed, default)
bindsym $mod+s layout stacking
bindsym $mod+t layout tabbed
bindsym $mod+e layout default

# client borders
bindsym $mod+i border normal
bindsym $mod+y border 1pixel
bindsym $mod+u border none
bindsym $mod+b border toggle

# volume control
bindsym XF86AudioLowerVolume exec amixer -q set Master 2- unmute
bindsym XF86AudioRaiseVolume exec amixer -q set Master 2+ unmute
bindsym $mod+XF86AudioLowerVolume exec amixer -q set Master 20- unmute
bindsym $mod+XF86AudioRaiseVolume exec amixer -q set Master 100+ unmute

# toggle tiling / floating
bindsym $mod+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+Shift+space focus mode_toggle

# focus the parent container
bindsym $mod+p focus parent

# focus the child container
bindsym $mod+d focus child

# scratchpad
bindsym $mod+a move scratchpad
bindsym $mod+z scratchpad show

# switch to workspace
bindsym $alt+1 workspace 1
bindsym $alt+2 workspace 2
bindsym $alt+3 workspace 3
bindsym $alt+4 workspace 4
bindsym $alt+5 workspace 5
bindsym $alt+6 workspace 6
bindsym $alt+7 workspace 7
bindsym $alt+8 workspace 8
bindsym $alt+9 workspace 9
bindsym $alt+0 workspace 10

# move focused container to workspace
bindsym $alt+Shift+1 move workspace 1
bindsym $alt+Shift+2 move workspace 2
bindsym $alt+Shift+3 move workspace 3
bindsym $alt+Shift+4 move workspace 4
bindsym $alt+Shift+5 move workspace 5
bindsym $alt+Shift+6 move workspace 6
bindsym $alt+Shift+7 move workspace 7
bindsym $alt+Shift+8 move workspace 8
bindsym $alt+Shift+9 move workspace 9
bindsym $alt+Shift+0 move workspace 10

# reload the configuration file
bindsym $alt+c reload

# exit i3 (logs you out of your X session)
bindsym $alt+Shift+q exit

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # They resize the border in the direction you pressed, e.g.
        # when pressing left, the window is resized so that it has
        # more space on its left

	bindsym Left resize shrink left 10 px or 10 ppt
	bindsym Shift+Left resize grow   left 10 px or 10 ppt

	bindsym Down resize shrink down 10 px or 10 ppt
	bindsym Shift+Down resize grow   down 10 px or 10 ppt

	bindsym Up resize grow   down 10 px or 10 ppt
	bindsym Shift+Up resize shrink down 10 px or 10 ppt

	#bindsym Up resize shrink up 10 px or 10 ppt
	#bindsym Shift+Up resize grow   up 10 px or 10 ppt

	bindsym Right resize shrink right 10 px or 10 ppt
	bindsym Shift+Right resize grow   right 10 px or 10 ppt

        # back to normal: Enter or Escape
	bindsym Return mode "default"
	bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
	font -*-fixed-medium-r-*-*-*-120-*-*-*-80-iso8859-1
	position top
	status_command ~/bin/conky-i3bar
	colors {
	    background #000000
	    statusline #cccccc

	    focused_workspace  #cccccc #000000
	    active_workspace   #888888 #000000
	    inactive_workspace #888888 #000000
	    urgent_workspace   #cccccc #660000
        }
}

# class                  border  backgr. text	 indicator
client.focused           #dddddd #dddddd #000000 #dddddd
client.focused_inactive  #999999 #333333 #aaaaaa #999999
client.unfocused         #777777 #222222 #aaaaaa #777777
client.urgent            #FF5555 #885555 #000000 #FF5555

# executes
bindsym $mod+$alt+0 exec $term -title "mcabber" -e mcabber
bindsym $mod+$alt+1 exec $term -title "irssi" -e irssi
bindsym $mod+$alt+2 exec $term -title "mutt" -e mutt -y
bindsym $mod+$alt+3 exec dwb
bindsym $mod+$alt+4 exec $term -title "rss" -e newsbeuter

bindsym $mod+3 exec java -Xmx512m -jar /home/geir/bin/JBidwatcher.jar
bindsym $mod+4 exec wesnoth
bindsym $mod+5 exec gimp
bindsym $mod+6 exec libreoffice
bindsym $mod+7 exec evince
bindsym $mod+8 exec free42bin
bindsym $mod+Shift+8 exec wine ~/.wine/drive_c/Program\ Files\ \(x86\)/Hewlett-Packard/HP\ Prime\ Virtual\ Calculator/HPPrime.exe
bindsym $mod+9 exec nonpareil 38c.kml
bindsym $mod+Shift+9 exec HP-15C
bindsym $mod+0 exec spotify

bindsym $mod+F1 exec i3lock -c 000000
bindsym $mod+F2 exec i3suspend.sh
bindsym $mod+F3 restart
bindsym $mod+F4 exec /home/geir/bin/wallpapers.sh
bindsym Control+F1 exec xbacklight -set 10
bindsym Control+F2 exec xbacklight -set 35
bindsym Control+F3 exec xbacklight -set 95
bindsym Control+F4 exec xrandr --auto
bindsym Control+F5 exec /home/geir/bin/mail_fetch.rb -n
bindsym shift+F5 exec rm .mail.lock
bindsym Control+F6 exec amixer set Master mute
bindsym shift+F6 exec /home/geir/bin/unmute.sh
bindsym Control+F7 exec /home/geir/bin/vol40.sh
bindsym Control+F8 exec /home/geir/bin/vol100.sh
bindsym Control+F9 exec samsung-tools -b toggle
bindsym Control+F10 exec brightkhigh
bindsym Control+F11 exec samsung-tools -c silent
bindsym Control+F12 exec samsung-tools -W toggle
bindsym Control+shift+F12 exec /usr/sbin/service network-manager restart

# assign to workspaces
assign [class="^URxvt$" title="^mcabber$"] → 1
assign [class="^URxvt$" title="^irssi$"] → 1
assign [class="^URxvt$" title="^mutt$"] → 2
assign [class="^dwb"] → 3
assign [class="^Uzbl-tabbed"] → 3
assign [class="^luakit"] → 3
assign [class="^Firefox"] → 3
assign [class="^Google-chrome"] → 3
assign [class="^URxvt$" title="^rss$"] → 4
assign [class="^URxvt$" title="^Root$"] → 5
assign [class="^URxvt$" title="^Files$"] → 5
assign [class="^URxvt$" title="^Geir$"] → 5

# floating apps
for_window [class="Free42bin"] floating enable
for_window [class="Nonpareil"] floating enable
for_window [class="Wine"] floating enable
for_window [class="Main.tcl"] floating enable

# Exec at startup
exec $term -title "irssi" -e irssi
exec $term -title "mcabber" -e mcabber
exec $term -title "mutt" -e mutt -y
exec $term -title "rss" -e newsbeuter
exec $term -title "Geir"
exec $term -title "Files" -e vifm
exec $term -title "Root" -e sudo -s
exec dwb
#exec gnome-power-manager
exec nm-applet
exec dropbox start

# Exec also at reload
exec_always /home/geir/bin/wallpapers.sh
