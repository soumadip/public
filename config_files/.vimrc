syntax on
filetype indent plugin on
set modeline
:highlight Comment ctermfg=green
:set mouse=i
:set hlsearch
:set incsearch
